package com.bpk.cypher;

/**
 * User: bpk
 * Date: 12.01.13
 * Time: 13:37
 */
public class CypherMatrix {
    byte[][] matrix = new byte[16][16];

    public CypherMatrix() {
        init();
    }

    public byte[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(byte[][] matrix) {
        this.matrix = matrix;
    }

    private void init() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; matrix.length > 0 && j < matrix[0].length; j++) {
                int byteNumber = i * 16 + j;
                matrix[i][j] = (byte) byteNumber;
            }
        }
    }

    public void performTranslation(String translationVector) {
        String matrixVector16Chars = prepareVectorToTranslation(translationVector, 16);
        for (int i = 0; i < matrixVector16Chars.length(); i += 2) {
            moveDown((int) matrixVector16Chars.charAt(i), i);
            moveRight((int) matrixVector16Chars.charAt(i + 1), i + 1);
        }
    }

    private String prepareVectorToTranslation(String inputText, int length) {
        if (inputText.length() >= length) {
            return inputText.substring(0, length);
        } else {
            StringBuilder stringBuilder = new StringBuilder();
            do {
                if (stringBuilder.length() + inputText.length() < length) {
                    stringBuilder.append(inputText);
                } else {
                    stringBuilder.append(inputText.substring(0, length - stringBuilder.length()));
                }
            } while (stringBuilder.length() < length);
            return stringBuilder.toString();
        }
    }

    private void moveDown(int numberOfPlaces, int columnNumber) {
        if (matrix != null && matrix.length != 0) {
            byte[] column = getColumn(columnNumber);
            for (int i = 0; i < column.length; i++) {
                matrix[(i + numberOfPlaces) % matrix.length][columnNumber] = column[i];
            }
        }
    }

    private byte[] getColumn(int columnNumber) {
        byte[] charColumn = new byte[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            charColumn[i] = matrix[i][columnNumber];
        }

        return charColumn;
    }

    private byte[] getRow(int rowNumber) {
        byte[] row = new byte[matrix[rowNumber].length];
        for (int i = 0; i < matrix[rowNumber].length; i++) {
            row[i] = matrix[rowNumber][i];
        }
        return row;
    }

    public void moveRight(int numberOfPlaces, int rowNumber) {
        if (matrix.length >= rowNumber) {
            byte[] row = getRow(rowNumber);
            for (int i = 0; i < row.length; i++) {
                matrix[rowNumber][(i + numberOfPlaces) % matrix[rowNumber].length] = row[i];
            }
        }
    }

    /**
     * For debug functions only
     */
    public void printMatrix() {
        for (int k = 0; k < matrix.length; k++) {
            for (int j = 0; j < matrix[k].length; j++) {
                System.out.print(String.format("%3s,", (int) (matrix[k][j])));
            }
            System.out.println();
        }
    }

    public byte getCodedChar(byte c) {
        byte result = (byte) 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (c == matrix[i][j]) {
                    return getCodedCharFromIndex(i, j);
                }
            }
        }
        return result;
    }

    private byte getCodedCharFromIndex(int row, int col) {
        int codedRow = matrix.length - 1 - row;
        int codedCol = matrix[row].length - 1 - col;
        return matrix[codedRow][codedCol];
    }
}
