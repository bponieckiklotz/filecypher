package com.bpk.cypher;

import org.junit.Test;

/**
 * User: bpk
 * Date: 09.01.13
 * Time: 21:19
 */
public class CypherTest {


    @Test
    public void testCypherFile() throws Exception {
//        Cypher cyper1 = new Cypher();
//        Cypher cyper2 = new Cypher();
//        cyper1.cypherFile("open1.txt", "textFile_discrit.txt", "abcdefg");
//        cyper2.decypherFile("textFile_discrit.txt", "textFile_anotherPublic.txt", "abcdefg");

        String s1 = "wqeaaaqwewqe";
        String s2 = "wqeaaqwaqwewqe";
        System.out.println(checkString(s1));
        System.out.println(checkString(s2));

    }

    private boolean checkString(String str) {
        for (int i = 2; i < str.length(); i++) {
            char char1 = str.charAt(i - 2);
            char char2 = str.charAt(i - 1);
            char char3 = str.charAt(i);
            if (char1 == char2 && char2 == char3) {
                return false;
            }
        }
        return true;
    }
}
