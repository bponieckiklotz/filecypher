package com.bpk.cypher;

import org.junit.Test;

/**
 * User: bpk
 * Date: 12.01.13
 * Time: 17:35
 */
public class CypherMatrixTest {

    @Test
    public void testPerformTranslation() throws Exception {
        CypherMatrix cypherMatrix = new CypherMatrix();
        cypherMatrix.printMatrix();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append((char) 1);
        stringBuilder.append((char) 2);
        cypherMatrix.performTranslation(stringBuilder.toString());
    }

//    @Test
//    public void testPrepareVectorToTranslation() throws Exception {
//        CypherMatrix cypherMatrix = new CypherMatrix();
//        int translationVectorLength = 16;
//        String s = cypherMatrix.prepareVectorToTranslation("abcds", translationVectorLength);
//        Assert.assertEquals(s, "abcdsabcdsabcds");
//        Assert.assertEquals(s.length(), translationVectorLength);
//    }

//    @Test
//    public void testMoveDown() {
//        CypherMatrix cypherMatrix = new CypherMatrix();
//        cypherMatrix.moveDown(1, 0);
//        char c = 'a';
//        System.out.println((int) c);
//    }

//    @Test
//    public void testMoveRight() {
//        CypherMatrix cypherMatrix = new CypherMatrix();
//        cypherMatrix.printMatrix();
//        System.out.println("-------------------------------");
//        cypherMatrix.moveRight(1, 0);
//        cypherMatrix.printMatrix();
//    }

//    @Test
//    public void testGetCodedCharFromIndex() {
//        CypherMatrix cypherMatrix = new CypherMatrix();
//        cypherMatrix.printMatrix();
//        System.out.println((int) cypherMatrix.getMatrix()[15][15]);
//        System.out.println((int) cypherMatrix.getCodedCharFromIndex(15, 15));
//    }
}
