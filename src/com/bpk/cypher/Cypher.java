package com.bpk.cypher;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

/**
 * User: bpk
 * Date: 09.01.13
 * Time: 20:17
 */
public class Cypher {

    public static final int BLOCK_SIZE = 5;

    public void cypherFile(String sourceFileName, String destinationFileName, String password) throws IOException {
        //read file into CypherBlock each with BLOCK_SIZE length
        List<CypherBlock> cypherBlocks = readFileIntoLines(new File(sourceFileName));
        List<CypherBlock> cipheredBlocks = new LinkedList<CypherBlock>();
        CypherMatrix cypherMatrix = new CypherMatrix();
        for (CypherBlock cypherBlock : cypherBlocks) {
            //cypher single block
            cypherMatrix.performTranslation(password);
            cipheredBlocks.add(cypherBlock.cypherBlock(cypherMatrix));
        }
        cypherMatrix.printMatrix();
        writeDestinationFile(cipheredBlocks, destinationFileName);
    }

    public void doNothingButSave(String sourceFileName, String destinationFileName) throws IOException {
        List<CypherBlock> cypherBlocks = readFileIntoLines(new File(sourceFileName));
        writeDestinationFile(cypherBlocks, destinationFileName);
    }

    public void decypherFile(String sourceFileName, String destinationFileName, String password) throws IOException {
        List<CypherBlock> cypheredBlocks = readFileIntoLines(new File(sourceFileName));
        List<CypherBlock> decipheredBlocks = new LinkedList<CypherBlock>();
        CypherMatrix cypherMatrix = new CypherMatrix();
        for (CypherBlock cypherBlock : cypheredBlocks) {
            //decypher single block
            cypherMatrix.performTranslation(password);
            decipheredBlocks.add(cypherBlock.cypherBlock(cypherMatrix));
        }
        writeDestinationFile(decipheredBlocks, destinationFileName);
    }

    private void writeDestinationFile(List<CypherBlock> cypherBlocks, String fileNameDestination) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(fileNameDestination));

        //create table to write
        byte[] byteArray = new byte[getTextLength(cypherBlocks)];
        //fill table
        int counter = 0;
        for (CypherBlock cypherBlock : cypherBlocks) {
            for (int i = 0; i < cypherBlock.getBytesFilled(); i++, counter++) {
                byteArray[counter] = cypherBlock.getByteBlock()[i];
            }
        }
        //write file from byte array
        try {
            dataOutputStream.write(byteArray);
        } finally {
            dataOutputStream.close();
        }
    }

    private int getTextLength(List<CypherBlock> cypherBlocks) {
        int count = 0;
        for (CypherBlock cypherBlock : cypherBlocks) {
            count += cypherBlock.getBytesFilled();
        }
        return count;
    }

    private List<CypherBlock> readFileIntoLines(File file) throws IOException {
        List<CypherBlock> cypherBlocks = new LinkedList<CypherBlock>();

        FileInputStream fileInputStream = new FileInputStream(file);
        DataInputStream dataInputStream = new DataInputStream(fileInputStream);
        try {
            int count = fileInputStream.available();
            if (count != 0) {
                byte[] byteArray = new byte[count];
                dataInputStream.read(byteArray);
                //counts how many blocks there will be
                int numberOfBlocks = count / BLOCK_SIZE;
                //adds full blocks
                for (int i = 0; i < numberOfBlocks; i++) {
                    int indexFrom = i * BLOCK_SIZE;
                    int indexTo = (i + 1) * BLOCK_SIZE;
                    cypherBlocks.add(prepareTextBlock(byteArray, indexFrom, indexTo));
                }
                //adds rest - last not full block
                if (count % BLOCK_SIZE != 0) {
                    int indexTo = numberOfBlocks * BLOCK_SIZE + (count % BLOCK_SIZE);
                    cypherBlocks.add(prepareTextBlock(byteArray, numberOfBlocks * BLOCK_SIZE, indexTo));
                }
            }
        } finally {
            dataInputStream.close();
        }
        return cypherBlocks;
    }

    private CypherBlock prepareTextBlock(byte[] byteArray, int indexFrom, int indexTo) {
        CypherBlock cypherBlock = new CypherBlock();
        for (int i = indexFrom; i < indexTo; i++) {
            cypherBlock.addByte(byteArray[i]);
        }
        return cypherBlock;
    }
}
