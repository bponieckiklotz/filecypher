package com.bpk.cypher;

/**
 * User: bpk
 * Date: 09.01.13
 * Time: 20:55
 */
public class CypherBlock {

    byte[] byteBlock = new byte[Cypher.BLOCK_SIZE];
    int bytesFilled = 0;

    public byte[] getByteBlock() {
        return byteBlock;
    }

    public void setByteBlock(byte[] byteBlock) {
        this.byteBlock = byteBlock;
    }

    public int getBytesFilled() {
        return bytesFilled;
    }

    public void setBytesFilled(int bytesFilled) {
        this.bytesFilled = bytesFilled;
    }

    public void addByte(byte byteToAdd) {
        byteBlock[bytesFilled] = byteToAdd;
        bytesFilled++;
    }

    public CypherBlock cypherBlock(CypherMatrix cypherMatrix) {
        CypherBlock cypherBlock = new CypherBlock();
        //cypher block based on Matrix
        for (int i = 0; i < bytesFilled; i++) {
            byte original = byteBlock[i];
            byte coded = cypherMatrix.getCodedChar(original);
            cypherBlock.addByte(coded);
        }
        return cypherBlock;
    }
}
