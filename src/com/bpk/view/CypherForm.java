package com.bpk.view;

import com.bpk.cypher.Cypher;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * User: bpk
 * Date: 29.12.12
 * Time: 22:01
 */
public class CypherForm {
    private JTextField sourceFilePath;
    private JTextField destinationFilePath;
    private JButton cypherButton;
    private JButton infoButton;
    private JButton decypherButton;
    private JPanel mainPanel;
    private JTextField password;

    private MessageProvider messageProvider = new MessageProvider();

    private Cypher cypher = new Cypher();

    private void createUIComponents() {
        mainPanel = new JPanel();
        sourceFilePath = new JTextField();
        password = new JTextField();
        destinationFilePath = new JTextField();
        cypherButton = new JButton();
        infoButton = new JButton();
        decypherButton = new JButton();
        initActionListeners();
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    private void initActionListeners() {
        cypherButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (checkPassword()) {
                    try {
                        cypher.cypherFile(sourceFilePath.getText(), destinationFilePath.getText(), password.getText());
                        JOptionPane.showMessageDialog(mainPanel, messageProvider.getMessageFromFile(MessageProvider.MSG_CYPHERED_CORRECT));
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(mainPanel, messageProvider.getMessageFromFile(MessageProvider.MSG_CYPHERED_ERROR));
                    }
                } else {
                    JOptionPane.showMessageDialog(mainPanel, messageProvider.getMessageFromFile(MessageProvider.MSG_PASSWORD_VALIDATION_FAILED));
                }
            }
        });
        decypherButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (checkPassword()) {
                    try {
                        cypher.decypherFile(sourceFilePath.getText(), destinationFilePath.getText(), password.getText());
                        JOptionPane.showMessageDialog(mainPanel, messageProvider.getMessageFromFile(MessageProvider.MSG_DECYPHERED_CORRECT));
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(mainPanel, messageProvider.getMessageFromFile(MessageProvider.MSG_DECYPHERED_ERROR));
                    }
                } else {
                    JOptionPane.showMessageDialog(mainPanel, messageProvider.getMessageFromFile(MessageProvider.MSG_PASSWORD_VALIDATION_FAILED));
                }
            }
        });
        infoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(mainPanel, messageProvider.getMessageFromFile(MessageProvider.MSG_INFO), "Informacje", JOptionPane.PLAIN_MESSAGE);
            }
        });
    }

    public boolean checkPassword() {
        String passwordText = password.getText();
        if (passwordText.length() < 3 || passwordText.length() > 16) {
            return false;
        }
        return validPassword(passwordText);
    }

    private boolean validPassword(String pass) {
        for (int i = 2; i < pass.length(); i++) {
            char char1 = pass.charAt(i - 2);
            char char2 = pass.charAt(i - 1);
            char char3 = pass.charAt(i);
            if (char1 == char2 && char2 == char3) {
                return false;
            }
        }
        return true;
    }
}
