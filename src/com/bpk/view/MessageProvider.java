package com.bpk.view;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * User: bpk
 * Date: 13.01.13
 * Time: 22:12
 */
public class MessageProvider {

    public static final String MESSAGES_FOLDER = "messages";
    public static final String MSG_PASSWORD_VALIDATION_FAILED = "passwordValidationFailed.txt";
    public static final String MSG_INFO = "info.txt";
    public static final String MSG_CYPHERED_CORRECT = "cypheredCorrect.txt";
    public static final String MSG_CYPHERED_ERROR = "cypheredError.txt";
    public static final String MSG_DECYPHERED_CORRECT = "decypheredCorrect.txt";
    public static final String MSG_DECYPHERED_ERROR = "decypheredError.txt";

    public String getMessageFromFile(String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            FileInputStream fileInputStream = new FileInputStream(MESSAGES_FOLDER + "/" + fileName);
            DataInputStream dataInputStream = new DataInputStream(fileInputStream);

            int count = fileInputStream.available();

            if (count != 0) {
                byte[] byteArray = new byte[count];
                dataInputStream.read(byteArray);
                stringBuilder.append(new String(byteArray));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
