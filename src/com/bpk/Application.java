package com.bpk;

import com.bpk.view.CypherForm;

import javax.swing.*;

/**
 * User: bpk
 * Date: 29.12.12
 * Time: 22:41
 */
public class Application {

    JFrame frame;

    Application(){
        frame = new JFrame("Cypher");
        frame.setContentPane(new CypherForm().getMainPanel());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        Application application = new Application();
    }
}
